/*
 * Created on 2005-mar-10
 *
 */
package tests;

import lang.ast.*;
import junit.framework.TestCase;

/**
 * @author gorel
 *
 */
public class BinaryNumberTests extends TestCase {
  double precision = 0.000000000001;
  public void testIntegralNumbers() {
    // 0
    BinaryNumber ast = new IntegralNumber(new SingularBitList(new Zero()));
    assertEquals(0.0, ast.value(), precision);
    
    // 1
    ast = new IntegralNumber(new SingularBitList(new One()));
    assertEquals(1.0, ast.value(), precision);
    
    // 101
    ast = new IntegralNumber(new PluralBitList(new PluralBitList(new SingularBitList(new One()), new Zero()), new One()));
    assertEquals(5.0, ast.value(), precision);
  }
  
  public void testRationalNumbers() {
    // 0.1
    BinaryNumber ast = new RationalNumber(new SingularBitList(new Zero()), new SingularBitList(new One()));
    assertEquals(0.5, ast.value(), precision);
    
    // 0.01
    ast = new RationalNumber(new SingularBitList(new Zero()),
        new PluralBitList(new SingularBitList(new Zero()), new One()));
    assertEquals(0.25, ast.value(), precision);
    
    // 10.01
    ast = new RationalNumber(new PluralBitList(new SingularBitList(new One()), new Zero()), 
        new PluralBitList(new SingularBitList(new Zero()), new One()));
    assertEquals(2.25, ast.value(), precision);
    
    // 1101.01
    ast = new RationalNumber
        (
        new PluralBitList
          (
          new PluralBitList
            (
            new PluralBitList
              (new SingularBitList(new One()), new One()),
            new Zero()
            ),
          new One()
          ),
        new PluralBitList(new SingularBitList(new Zero()), new One())
        );
    assertEquals(13.25, ast.value(), precision);
  }

}
