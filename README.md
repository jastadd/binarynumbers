# JastAdd Implementation of Knuth's Binary Numbers #

The Binary Numbers example is from Knuth's seminal article on attribute grammars, [Semantics of Context-Free Languages, Math. Sys. Theory, 1968](https://doi.org/10.1007/BF01692511).

The example is very small and simple, and makes use of both inherited and synthesized attributes. This directory contains the corresponding JastAdd implementation.

## System requirements ##
You only need to have Java installed.

`Gradle` is used for building, but a wrapper script `gradlew` automatically downloads Gradle the first time you build. Gradle then downloads `JastAdd` and `Junit`.

## Build and run ##

Build and run the test cases:

    ./gradlew test

Clean up: Remove generated files:

    ./gradlew clean

### Notes on building ###

Gradle places all downloaded libraries in a dir `.gradle` in your home directory. A local dir `.gradle` contains information used internally by Gradle.

## Files to look at ##

* `build.gradle` - build file
* `src/jastadd/BinaryNumber.ast` - abstract grammar
* `src/jastadd/BinaryNumberValue.jrag` - attribution
* `src/test/BinaryNumberTests.java` - JUnit tests that create example ASTs and check attribute values.

Generated files:

* `src/gen/lang/ast/...` - generated Java AST node classes with attributes
* `build/...` - generated build products
* `build/reports/tests` - generated test reports


## Knuth's example explained ##

The example defines a binary number as an abstract syntax tree, with individual bits as individual nodes. Attributes are used for computing the corresponding decimal value.

### Context-free grammar ###

Knuth uses the following context-free grammar to define the structure of binary numbers

    N ::= L
    N ::= L "." L
    L ::= B
    L ::= L B
    B ::= "0"
    B ::= "1"
Examples of binary numbers following this grammar are integral numbers like `0, 1, 10, 11, 100`, and rational numbers with an integral and a fractional part, like `1.1, 1.01, 10.01`.

In the JastAdd abstract grammar, the nonterminals `N, L, and B`, are renamed to `BinaryNumber, BitList, and Bit`. Furthermore, the productions are given explicit names like `IntegralNumber`, `RationalNumber`, `SingularBitList`, `PluralBitList`, `Zero`, and `One`.


### Attribution ###

The idea is to compute the values of the binary numbers using an attribute grammar. Here are a number of test cases:

Binary number | Value (in decimal notation)
--------------| ---------------------------
0 | 0
1 | 1
101 | 5
0.1 | 0.5
0.01 | 0.25
10.01 | 2.25
1101.01 | 13.25

Knuth provides a couple of different attribute grammars to define the computation. The first one uses synthesized attributes only. The second one, which he argues is closer to the way people usually think about the binary notation, uses both inherited and synthesized attributes. The latter attribution is the one we will illustrate using JastAdd.

In this attribution, each bit has a synthesized attribute *value* which is a rational number that takes the position of the bit into account. For example, the first bit in the binary number 10.01 will have the value 2 and the last bit will have the value 0.25. In order to define these attributes, each bit also has an inherited attribute *scale* used to compute the value of a `One` bit as `value = 2**scale`. So for the bits in 10.01, scale will be 1, 0, -1, -2, respectively.

If we have the values of the individual bits, these values can simply be summed up to the total value. This is done by adding synthesized attributes *value* both for `BitList` and for `BinaryNumber`. In order to compute the scale attribute for the individual bits, an inherited attribute *scale* is added also for `BitList` (representing the scale of the rightmost bit in that list), and a synthesized attribute *length* for `BitList`, holding the length of the list.

Knuth uses the abbreviations *v*, *s*, and *l* for the *value*, *scale*, and *length* attributes. The table below shows Knuth's resulting attribute grammar.

Syntactic rules | Semantic rules
----------------| --------------
`B ::= "0"` | `v(B) = 0`
`B ::= "1"` | `v(B) = 2**s(B)`
|
`L ::= B` | `v(L) = v(B)`
          |`s(B) = s(L)`
          |`l(L) = 1`
|
`L1 ::= L2 B` | `v(L1) = v(L2) + v(B)`
              |`s(B) = s(L1)`
              |`s(L2) = s(L1 + 1)`
              |`l(L1) = l(L2) + 1`
|
`N ::= L` | `v(N) = v(L)`
          |`s(L) = 0`
|
`N ::= L1 "." L2` | `v(N) = v(L1) + v(L2)`
                  |`s(L1) = 0`
                  |`s(L2) = -l(L2)`

The JastAdd implementation uses exactly the same attributes and equations as in Knuth's example.
